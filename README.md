# SysCache

This modules allows Drupal caches to be stored in filesystem instead of database.


##Similar modules
  
https://www.drupal.org/project/staticfilecache  
https://www.drupal.org/project/cache_debug  


## Instalation

add to settings.php next lines

$conf['cache_backends'] = ['sites/all/modules/syscache/src/DrupalSysCache.php'];  
$conf['cache_default_class'] = 'DrupalSysCache';  
$conf['sys_directory'] = '/some/your/syscache';  