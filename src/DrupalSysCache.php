<?php
require_once dirname(__DIR__). '/vendor/autoload.php';

use Symfony\Component\Filesystem\Filesystem;


class SysCacheObjectDummy {
  public $cid;
  /**
   * 
   * @var int timestamp
   */
  public $created;
  
  /**
   * can be CACHE_PERMANENT | CACHE_TEMPORARY | timestamp
   * just LOL..
   * 
   * @var int
   */
  public $expire;
  public $data;
}


/**
 * Defines a filesystem cache implementation to be used
 *
 * This class is intiated per bin
 *
 * USAGE
 * 
 * add to settings.php
 * 
 * $conf['cache_backends'] = ['sites/all/modules/syscache/src/DrupalSysCache.php'];
 * $conf['cache_default_class'] = 'DrupalSysCache';
 * $conf['sys_directory'] = __DIR__ . '/syscache';
 */
class DrupalSysCache implements DrupalCacheInterface {
  
  protected $bin;
  protected $binDir;
  protected static $fs;

  private $profile_data;
  private function profile($message) {
    return;
    $this->profile_data[] = debug_backtrace()[1]['function'] ." : $message\n";
  }
  
  
  /**
   * Retuns filesystem helper
   * 
   * @return \Symfony\Component\Filesystem\Filesystem
   */
  protected static function getFS() {
    if (!(self::$fs instanceof Filesystem)) {
      self::$fs = new Filesystem();
    }
    return self::$fs;
  }

  public function __destruct() {
    $cacheDir = variable_get('syscache_directory');
    file_put_contents($cacheDir . '/_debug.txt', implode('', $this->profile_data), FILE_APPEND);
  }

  
  /**
   * @param string $bin bin name like 'cache_bootstrap'
   *        
   * Some bins: cache_bootstrap, cache_rules, cache, cache_field, cache_views, cache_path, cache_panels, cache_menu
   */
  public function __construct($bin) {
    $this->profile('init bin ' . $bin);
    $this->bin = $bin;
    // TODO: secure
    $syscacheDir = variable_get('syscache_directory');
    $this->binDir = $syscacheDir . '/' . $this->bin;
    
    if(!self::getFS()->exists($syscacheDir)) {
      self::getFS()->mkdir($syscacheDir);
      self::getFS()->dumpFile($syscacheDir.'/.htaccess', "Deny from all");
      $this->profile('created directory ' . $syscacheDir);
    }
    
    if(!self::getFS()->exists($this->binDir)) {
      self::getFS()->mkdir($this->binDir);
      $this->profile('created directory ' . $this->binDir);
    }
  }

  
  /**
   * {@inheritdoc}
   * @see DrupalCacheInterface::get()
   */
  public function get($cid) {
    $this->profile('cid ' . $cid);
    $filename = $this->filenameByCacheId($cid);
    
    if(self::getFS()->exists($filename)) {
      $this->profile('READ cid ' . $cid);
      $cache = unserialize(file_get_contents($filename));
      return $cache;
    }
    
    return FALSE;
  }

  
  /**
   * {@inheritdoc}
   * @see DrupalCacheInterface::getMultiple()
   */
  function getMultiple(&$cids) {
    $this->profile('cids ' . implode(',', $cids));
    $caches = [];
    foreach ($cids as $cid) {
      $cache = $this->get($cid);
      if ($cache !== false) {
        $caches[$cid] = $cache;
      }
    }
    $cids = array_diff($cids, array_keys($caches));
    
    return $caches;
  }

  
  /**
   * {@inheritdoc}
   * @see DrupalCacheInterface::set()
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $this->profile('cid ' . $cid);
    /** @var $cache SysCacheObjectDummy */
    $cache = new stdClass();
    $cache->cid = $cid;
    $cache->created = REQUEST_TIME;
    $cache->expire = $expire;
    $cache->data = $data;
    
    self::getFS()->dumpFile($this->filenameByCacheId($cache->cid), serialize($cache));
  }

  
  /**
   * {@inheritdoc}
   * @see DrupalCacheInterface::clear()
   */
  function clear($cid = NULL, $wildcard = FALSE) {
    /* @var $cache SysCacheObjectDummy */
    $this->profile('cid ' . $cid);
    
    // nothing to clear
    if (!self::getFS()->exists($this->binDir)) {
      return;
    }
    
    // -----------------------
    // variants
    // ----
    // [1] $cid = NULL, $wildcard = IGNORED                (remove temporary expired)
    //
    // [2] $cid = 'string', $wildcard = false
    // [3] $cid = ['string', 'string'], $wildcard = false
    //
    // [4] $cid = 'stri_*', $wildcard = true
    // [5] $cid = ['str_*', 'str2*'], $wildcard = true
    // [6] $cid = '*', $wildcard = true                   (remove everything)
    // -----------------------

    // 
    // [1] clear all expired non perma cache
    //
    if (empty($cid)) {
      $this->profile('[1]: cid ' . $cid);
      foreach (new DirectoryIterator($this->binDir) as $fileInfo) {
        if($fileInfo->isDot()) continue;
        $cache = unserialize(file_get_contents($fileInfo->getPathname()));
        $expire_date = ($cache->expire == CACHE_TEMPORARY || $cache->expire == CACHE_PERMANENT ? 
          $cache->created + variable_get('cache_lifetime', 0) :
          $cache->expire);
        if ($cache === false || // something was wrong with data
            ($cache->expire != CACHE_PERMANENT && $expire_date < REQUEST_TIME)) {
          self::getFS()->remove($fileInfo->getPathname());
          continue;
        }
      }
      return;
    } // [1]
    
    //
    // [2][3] clear particular cache by id
    //
    if (!empty($cid) && !$wildcard) {
      $this->profile('[2][3]: cid ' . $cid);
      // easier to look by keys
      $cids = is_array($cid) ? array_combine($cid, $cid) : [$cid=>$cid];
      foreach (new DirectoryIterator($this->binDir) as $fileInfo) {
        if($fileInfo->isDot()) continue;
        $cache = unserialize(file_get_contents($fileInfo->getPathname()));
        if ($cache === false) {
          // something was wrong with data, just remove it
          self::getFS()->remove($fileInfo->getPathname());
          continue;
        }
        if (in_array($cache->cid, $cids)) { // found by cid, remove and exit
          self::getFS()->remove($fileInfo->getPathname());
          unset($cids[$cache->cid]);
          if (empty($cids)) { // all is clean
            return;
          }
        }
      }
    }// [2], [3]

    //
    // [4][5][6] remove cache by matching cache id
    //
    if (!empty($cid) && $wildcard) {
      $this->profile('[4][5][6]: cid ' . $cid);
      if ($cid == '*') { // [6]
        self::getFS()->remove($this->binDir);
        return;
      }
      // easier to look by keys
      $cids = is_array($cid) ? array_combine($cid, $cid) : [$cid=>$cid];
      foreach (new DirectoryIterator($this->binDir) as $fileInfo) {
        if($fileInfo->isDot()) continue;
        $cache = unserialize(file_get_contents($fileInfo->getPathname()));
        if ($cache === false) {
          // something was wrong with data, just remove it
          self::getFS()->remove($fileInfo->getPathname());
          continue;
        }
        
        foreach ($cids as $_cid) {
          if (preg_match("/^$_cid/", $cache->cid)) { // found by cid, remove and exit
            self::getFS()->remove($fileInfo->getPathname());
            break;
          }
        }//foreach cids
      } // foreach files
    }// [4][5][6]
    
  }

  
  /**
   * {@inheritdoc}
   * @see DrupalCacheInterface::isEmpty()
   */
  function isEmpty() {
    $this->profile('');
    return !(new FilesystemIterator($this->binDir))->valid();
  }
  
  //TODO: add garbage colection 
  
  /**
   * gets hash name valid for using as filename
   *
   * @param unknown $cache        
   * @return string
   */
  protected function filenameByCacheId($cid) {
    return $this->binDir . '/' . hash('sha256', $cid);
  }

  
}

